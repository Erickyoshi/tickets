import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Ticket } from 'src/app/model/ticket.model';
import { Observable } from 'rxjs';
import { DefaultResponse } from 'src/app/model/response.model';
import { TicketService } from 'src/app/service/ticket.service';


@Component({
    selector: 'app-list-ticket',
    templateUrl: './list-ticket.component.html',
    styleUrls: ['./list-ticket.component.css'],
})

export class ListTicketsComponent implements OnInit{

    tickets: Ticket[] = []
    
    formSearch = this.formBuilder.group({
        searchType: [1, [Validators.required]],
        searchText: ['', [Validators.required]]
    })

    constructor(private formBuilder: FormBuilder,
                private ticketService: TicketService) {}

    ngOnInit(){
        this.findTickets()
    }

    findTickets(){
        this.ticketService.findAll().subscribe(
            response => this.onSearchResult(response)
        )
    }

    search() {
        let result: Observable<Ticket[]>
        if (this.searchType.value == 1) {
            result = this.ticketService.findByNameIssuer(this.searchText.value)
               
        }else{
            result = this.ticketService.findByNameResponder(this.searchText.value)
        }
        result.subscribe(response => this.onSearchResult(response))
    }

    onSearchResult(result: Ticket[]) {
        if (result != null) {
            this.tickets = result
        }
    }

    clearSearch() {
        this.searchText.setValue("")
        this.searchType.setValue(1)
        this.findTickets()
    }

    get searchType() {
        return this.formSearch.controls.searchType
    }

    get searchText() {
        return this.formSearch.controls.searchText
    }

}