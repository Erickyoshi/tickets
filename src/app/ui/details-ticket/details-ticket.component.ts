import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TicketService } from 'src/app/service/ticket.service';
import { Ticket } from 'src/app/model/ticket.model';

@Component({
    selector: 'app-details-ticket',
    templateUrl: './details-ticket.component.html',
    styleUrls: ['./details-ticket.component.css']
})

export class DetailsTicketComponent implements OnInit{

    
    ticket: Ticket = new Ticket()
    constructor(private route:ActivatedRoute,
                private service: TicketService) { }


    ngOnInit(): void {
        this.route.params.subscribe(params => {
            let ticketId = params['ticket_nameissuer']
            this.findTicket(ticketId)
    })

}

    findTicket(ticketId: number){
        this.service.findById(ticketId).subscribe(
            result => {
                this.ticket = result
            }
        )
    }

}