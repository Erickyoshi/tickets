import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterTicket } from 'src/app/model/ticket.model';
import { TicketService } from 'src/app/service/ticket.service';

@Component({
    selector: 'app-register-ticket',
    templateUrl: './register-ticket.component.html',
    styleUrls: ['./register-ticket.component.css']
})

export class RegisterTicketComponent{


    formTicket = this.formBuilder.group({
        name_issuer:['', [Validators.required]],
        email_issuer: ['', [ Validators.required, Validators.email ] ],
        name_responder: ['', [ Validators.required ] ],
        email_responder: ['', [ Validators.required, Validators.email ] ],
        title: ['', [ Validators.required ] ],
        description: ['', [ Validators.required ] ],
        openDate: [Date],
        status: [1],
        closeDate: [Date]

    })

    constructor(private formBuilder: FormBuilder,
        private router: Router, 
        private service: TicketService) { }

    save() {
            let ticket = new RegisterTicket(
                this.name_issuer.value,
                this.email_issuer.value,
                this.name_responder.value,
                this.email_responder.value,
                this.title.value,
                this.description.value,
                this.openDate.value(new Date),
                this.status.value(1),
                this.closeDate.value
            )   

            this.service.save(ticket).subscribe(
                result => this.cancel()
            )
        }


    cancel() {
        this.router.navigateByUrl("/")
    }

    get name_issuer() {
        return this.formTicket.controls.name_issuer
    }

    get email_issuer() {
        return this.formTicket.controls.email_issuer
    }

    get name_responder() {
        return this.formTicket.controls.name_responder
    }

    get email_responder() {
        return this.formTicket.controls.email_responder
    }

    get title() {
        return this.formTicket.controls.title
    }

    get description() {
        return this.formTicket.controls.description
    }

    get openDate() {
        return this.formTicket.controls.openDate
    }

    get status() {
        return this.formTicket.controls.status
    }

    get closeDate() {
        return this.formTicket.controls.closeDate
    }
}