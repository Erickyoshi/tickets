import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, pipe } from "rxjs";
import { map} from "rxjs/operators";
import { Ticket, RegisterTicket} from "../model/ticket.model"
import { DefaultResponse} from "../model/response.model"

const URL_BASE = "http://localhost:8080/tickets/"
const URL_FIND_BY_NAME_ISSUER = URL_BASE + "byNameIssuer?nameIssuer="
const URL_FIND_BY_NAME_RESPONDER = URL_BASE + "byNameResponder/"

@Injectable({
    providedIn: 'root'
})


export class TicketService {

    constructor(private http: HttpClient) {

    }

    findAll(): Observable<Ticket[]>{
        return this.http.get< DefaultResponse<Ticket[]>>(URL_BASE).pipe(
            map(response => response.result)
        )
    }

    findByNameIssuer(nameIssuer: string): Observable<Ticket[]> {
        return this.http.get< DefaultResponse<Ticket[]> >(URL_FIND_BY_NAME_ISSUER + nameIssuer).pipe(
            map(response => response.result)
        )
    }

    findByNameResponder(nameResponder: string): Observable<Ticket[]> {
        return this.http.get< DefaultResponse<Ticket[]> >(URL_FIND_BY_NAME_RESPONDER + nameResponder).pipe(
            map(response => response.result)
        )
    }

    findById(id: number){
        return this.http.get<DefaultResponse<Ticket>>(URL_BASE + id).pipe(
            map(response => response.result)
        )
    }


    save(ticket: RegisterTicket): Observable<boolean>{
        return this.http.post<DefaultResponse<boolean>>(URL_BASE, ticket).pipe(
            map(response => response.result)
        )
    }

}