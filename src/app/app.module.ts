import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './ui/home/app.component';


import { ListTicketsComponent } from './ui/list-ticket/list-ticket.component';
import { RegisterTicketComponent } from './ui/register-ticket/register-ticket.component';
import { DetailsTicketComponent } from './ui/details-ticket/details-ticket.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    ListTicketsComponent,
    RegisterTicketComponent,
    DetailsTicketComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
