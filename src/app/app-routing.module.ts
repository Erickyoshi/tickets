import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsTicketComponent} from './ui/details-ticket/details-ticket.component';
import { ListTicketsComponent } from './ui/list-ticket/list-ticket.component';
import { RegisterTicketComponent } from './ui/register-ticket/register-ticket.component';

const routes: Routes = [
    { path: 'list-ticket', component: ListTicketsComponent },
    { path: 'register-ticket', component: RegisterTicketComponent },
    { path: 'details-ticket/:ticket_id', component: DetailsTicketComponent },
    { path: '', pathMatch: 'full', redirectTo: '/list-ticket'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
